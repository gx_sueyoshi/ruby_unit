module RubyUnit
  class StopWatch

    def initialize
      @started_at = 0
      @finished_at = 0
    end

    def time
      @finished_at - @started_at
    end

    def started
      @started_at = Time.now.to_f
    end

    def finished
      @finished_at = Time.now.to_f
    end
  end
end
