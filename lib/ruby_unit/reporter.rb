module RubyUnit
  class Reporter

    def initialize(stop_watch = StopWatch.new)
      @stop_watch = stop_watch
    end

    def report(test_file_path, test_result)
      output = []
      output << 'PASS'
      output << "#{test_result[:tests]} tests in #{test_file_path}"
      output << "#{test_result[:failures]} failures"
      output << ''
      output.join("\n")
    end
  end
end
