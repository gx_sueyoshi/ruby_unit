require 'pathname'

module RubyUnit
  class TestFile
    attr_reader :path

    def initialize(path)
      @path = path
      @filename = extract_filename(path)
    end

    def test_case_classname
      @filename
        .split('_')
        .map { |word| word.capitalize }
        .join
    end

    def ==(other)
      other.instance_of?(self.class) &&
        self.path == other.path
    end

    private

      def extract_filename(path)
        Pathname.new(path)
          .basename('.rb')
          .to_s
      end
  end
end
