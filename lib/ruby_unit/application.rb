module RubyUnit
  class Application

    def initialize(test_case_loader = TestCaseLoader.new, reporter = Reporter.default)
      @test_case_loader = test_case_loader
      @reporter = reporter
    end

    def run(lib_dir_path, test_file_path)
      test_file = TestFile.new(test_file_path)
      test_case = @test_case_loader.load(lib_dir_path, test_file)
      test_result = test_case.run
      @reporter.report(test_file_path, test_result)
    end
  end
end
