module RubyUnit
  class CLI

    def initialize(application)
      @application = application
    end

    def run(args)
      lib_dir_path, test_file_path = parse_arguments(args)
      report = @application.run(lib_dir_path, test_file_path)
      puts report
    end

    private

      def parse_arguments(args)
        args[1..2]
      end
  end
end
