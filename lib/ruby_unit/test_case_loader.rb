require 'pathname'

module RubyUnit
  class TestCaseLoader

    def load(lib_dir_path, test_file)
      $LOAD_PATH << lib_dir_path
      Kernel.require(test_file.path)
      test_case_class = Module.new.const_get(test_file.test_case_classname)
      test_case_class.new
    end
  end
end
