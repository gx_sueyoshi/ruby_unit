module RubyUnit
  class TestCase
    class << self

      def test_methods
        instance_methods.select { |method| method =~ /^test_/ }
      end
    end

    def run
      run_all_test_methods
      {
        tests: all_test_methods.size,
        failures: 0
      }
    end

    private

      def run_all_test_methods
        all_test_methods.each do |test_method|
          send(test_method)
        end
      end

      def all_test_methods
        self.class.test_methods
      end
  end
end
