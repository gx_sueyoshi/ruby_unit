require 'spec_helper'

describe 'テストが1つ（アサーションなし）の単体のテストファイルを実行する', feature: true do
  include CommandRunner

  let(:stop_watch) { FixedStopWatch.new(0.1, 0.2) }
  let(:lib_dir_path) { fixture_path('lib') }
  let(:test_file_path) { fixture_path('test/test_one_test_no_assertion.rb') }

  let(:expected_output) do
    <<-EOO
PASS
1 tests in #{test_file_path}
0 failures
    EOO
  end

  it 'テストを実行し、テストファイルの情報と結果を表示する' do
    expect {
      Command(stop_watch).run('-l', lib_dir_path, test_file_path)
    }.to output(expected_output).to_stdout

    expect(TestLogger.get(:one_test_no_assertion)).to eq([:test_method_called])
  end
end
