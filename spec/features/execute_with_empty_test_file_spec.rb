require 'spec_helper'

describe 'テストが書かれていない単体のテストファイルを実行する', feature: true do
  include CommandRunner

  let(:stop_watch) { FixedStopWatch.new(0.1, 0.2) }
  let(:lib_dir_path) { fixture_path('lib') }
  let(:test_file_path) { fixture_path('test/test_empty.rb') }

  let(:expected_output) do
    <<-EOO
PASS
0 tests in #{test_file_path}
0 failures
    EOO
  end

  it 'テストは実行せずにテストファイルの情報と空の結果を表示する' do
    expect {
      Command(stop_watch).run('-l', lib_dir_path, test_file_path)
    }.to output(expected_output).to_stdout
    expect(TestLogger.get(:test_empty)).to eq([:file_loaded])
  end
end
