require 'ruby_unit'
require 'system_under_test'

class TestSystemUnderTest < RubyUnit::TestCase

  def setup
    @sut = SystemUnderTest.new
  end

  def teardown
    remove_instance_variable(:@sut)
  end

  def test_bar
    actual = @sut.do_something
    expected = 'something done'
    assert_equal(actual, expected)
  end
end
