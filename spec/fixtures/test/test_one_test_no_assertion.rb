require 'test_logger'

class TestOneTestNoAssertion < RubyUnit::TestCase

  def test_method
    TestLogger.log(:one_test_no_assertion, :test_method_called)
  end
end
