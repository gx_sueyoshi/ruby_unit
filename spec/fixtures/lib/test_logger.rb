require 'singleton'

class TestLogger
  include Singleton

  class << self

    def get(*args)
      instance.get(*args)
    end

    def log(*args)
      instance.log(*args)
    end
  end

  attr_reader :logs

  def initialize
    @logs = Hash.new {|h, k| h[k] = [] }
  end

  def get(label)
    @logs[label]
  end

  def log(label, message)
    @logs[label] << message
  end
end
