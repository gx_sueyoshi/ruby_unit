require 'spec_helper'

module RubyUnit
  describe TestCaseLoader do
    describe '#load' do
      let(:lib_dir_path) { fixture_path('lib') }
      let(:test_file) { TestFile.new(test_file_path) }
      let(:test_file_path) { fixture_path('test/test_empty.rb') }

      it do
        loader = described_class.new
        test_case = loader.load(lib_dir_path, test_file)
        expect(test_case).to be_instance_of(TestEmpty)
      end
    end
  end
end
