require 'spec_helper'

module RubyUnit
  describe TestCase do
    describe '#run' do
      let(:test_case) { test_case_class.new }
      let(:result) { test_case.run }

      context 'no test method' do
        let(:test_case_class) { Class.new(described_class) }

        it do
          expect(result).to eq({ tests: 0, failures: 0 })
        end
      end

      context '1 test method no assertions' do
        let(:test_case_class) do
          Class.new(described_class) do
            attr_reader :called

            def test_foo
              @called = true
            end
          end
        end

        it do
          expect(result).to eq({ tests: 1, failures: 0 })
        end

        it do
          result
          expect(test_case.called).to be_truthy
        end
      end
    end
  end
end
