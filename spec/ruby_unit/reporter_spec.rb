require 'spec_helper'

module RubyUnit
  describe Reporter do
    describe '#report' do
      context 'given "/path/to/test_file.rb", { tests: 0, failures: 0 }' do
        let(:test_file_path) { '/path/to/test_file.rb' }
        let(:test_result) { { tests: 0, failures: 0 } }

        it do
          reporter = described_class.new
          report = reporter.report(test_file_path, test_result)
          expect(report).to eq(
            <<-EOS
PASS
0 tests in /path/to/test_file.rb
0 failures
            EOS
          )
        end
      end
    end
  end
end
