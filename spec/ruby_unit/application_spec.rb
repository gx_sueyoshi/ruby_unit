require 'spec_helper'

module RubyUnit
  describe Application do
    describe '#run' do
      context 'LIB_DIR_PATHとTEST_FILE_PATHが渡されたとき' do
        let(:test_case_loader) { double(:test_case_loader) }
        let(:reporter) { double(:reporter) }

        let(:test_file) { TestFile.new(test_file_path) }
        let(:test_case) { double(:test_case) }
        let(:test_result) { double(:test_result) }

        let(:lib_dir_path) { '/path/to/lib' }
        let(:test_file_path) { '/path/to/test_file.rb' }

        it do
          expect(test_case_loader)
            .to receive(:load)
            .with(lib_dir_path, test_file)
            .and_return(test_case)

          expect(test_case)
            .to receive(:run)
            .and_return(test_result)

          expect(reporter)
            .to receive(:report)
            .with(test_file_path, test_result)

          application = described_class.new(test_case_loader, reporter)
          application.run(lib_dir_path, test_file_path)
        end
      end
    end
  end
end
