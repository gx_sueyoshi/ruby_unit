require 'spec_helper'

module RubyUnit
  describe TestFile do
    describe '#test_case_classname' do
      context 'given /path/to/test_case.rb' do
        it do
          test_file = described_class.new('/path/to/test_case.rb')
          class_name = test_file.test_case_classname
          expect(class_name).to eq('TestCase')
        end
      end
    end
  end
end
