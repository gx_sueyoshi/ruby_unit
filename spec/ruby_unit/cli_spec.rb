require 'spec_helper'

module RubyUnit
  describe CLI do
    describe '#run' do
      let(:application) { double(:application) }

      context '`-l LIB_DIR_PATH TEST_FILE_PATH`が渡されたとき' do
        it do
          expect(application)
            .to receive(:run)
            .with('/path/to/lib', '/path/to/test_file.rb')

          cli = described_class.new(application)
          cli.run(%w|-l /path/to/lib /path/to/test_file.rb|)
        end
      end
    end
  end
end
