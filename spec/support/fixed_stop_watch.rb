class FixedStopWatch

  def initialize(will_start_at, will_finish_at)
    @will_start_at = will_start_at
    @will_finish_at = will_finish_at
  end

  def started
    @started_at = @will_start_at
  end

  def finished
    @finished_at = @will_finish_at
  end

  def time
    @finished_at - @started_at
  end
end
