module CommandRunner

  def Command(stop_watch = nil)
    Command.new(stop_watch)
  end

  class Command

    def initialize(stop_watch)
      @application = RubyUnit::Application.new(
        RubyUnit::TestCaseLoader.new,
        RubyUnit::Reporter.new(stop_watch)
      )
    end

    def run(*args)
      cli = RubyUnit::CLI.new(@application)
      cli.run(args)
    end
  end
end
