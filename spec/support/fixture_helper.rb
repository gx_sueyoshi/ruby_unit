module FixtureHelper
  FIXTURES_BASE_PATH = File.expand_path('../../fixtures', __FILE__).freeze

  def fixture_path(relative_path)
    FIXTURES_BASE_PATH + "/#{relative_path}"
  end
end
