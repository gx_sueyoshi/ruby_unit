# RubyUnit

## 実行結果

- 成功時

```bash
PASS
Finished in 0.00865 seconds
5 tests in /path/to/test_file.rb
0 failures
```

- 失敗時

```bash
FAILED
Finished in 0.00865 seconds
5 tests in /path/to/test_file.rb
2 failures
test_method_a
  expected to equal EXPECTED but was ACTUAL (Default Message)
  expected to be true but was false (Default Message)
test_method_b
  Not correct (Given Message to assetion method)
```
